﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class Status : EntityBase<Guid>
    {
        /// <summary>
        /// Id of status
        /// </summary>
        public override Guid Id { get; set; }

        /// <summary>
        /// Status name of store
        /// </summary>
        public string StatusName { get; set; }
    }
}