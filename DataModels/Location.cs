﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class Location : EntityBase<Guid>
    {
        /// <summary>
        /// Id of location
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        
        /// <summary>
        /// City name of the store
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// District name of the store
        /// </summary>
        public string District { get; set; }
        
        /// <summary>
        /// Ward name of the store
        /// </summary>
        public string Ward { get; set; }
    }
}