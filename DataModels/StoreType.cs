﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class StoreType : EntityBase<Guid>, ICommand
    {
        /// <summary>
        /// Id of store type
        /// </summary>
        public override Guid Id { get; set; }

        /// <summary>
        /// Store type name
        /// </summary>
        public string Type { get; set; }
    }
}