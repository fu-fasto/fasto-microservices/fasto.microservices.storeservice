﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class FoodRegion : EntityBase<Guid>
    {
        /// <summary>
        /// Id of food region
        /// </summary>
        public override Guid Id { get; set; }
        /// <summary>
        /// Name of food region
        /// </summary>
        public string RegionName { get; set; }
    }
}