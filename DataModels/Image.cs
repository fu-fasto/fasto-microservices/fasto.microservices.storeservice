﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class Image : EntityBase<Guid>
    {
        /// <summary>
        /// Id of image
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        /// <summary>
        /// Imgage url
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// StoreId of image
        /// </summary>
        public Guid StoreId { get; set; }
    }
}