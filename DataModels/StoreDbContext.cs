﻿using MicroBoost.Persistence.EfCore;
using Microsoft.EntityFrameworkCore;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class StoreDbContext : EfCoreContext
    {
        public DbSet<Store> Stores { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Item> Menus { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<FoodRegion> FoodRegions { get; set; }
        public DbSet<StoreType> StoreTypes { get; set; }
        
        public StoreDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}