﻿using System;
using System.Collections.Generic;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class Item: EntityBase<Guid>
    {
        /// <summary>
        /// Id of the item of menu
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        
        /// <summary>
        /// Name of item
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Price of item
        /// </summary>
        public double Price { get; set; }
        
        /// <summary>
        /// Some description about the item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Image of the item
        /// </summary>
        public string ImagesUrl { get; set; }
        
        /// <summary>
        /// Store Id of the item
        /// </summary>
        public Guid StoreId { get; set; }
    }
}