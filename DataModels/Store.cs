﻿using System;
using System.Collections.Generic;
using MicroBoost.Types;

namespace FastO.Microservices.Storeservice.DataModels
{
    public class Store : EntityBase<Guid>
    {
        /// <summary>
        /// Id of store
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Name of store
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Open time
        /// </summary>
        public DateTimeOffset OpenTime { get; set; }

        /// <summary>
        /// Close time
        /// </summary>
        public DateTimeOffset CloseTime { get; set; }

        /// <summary>
        /// Name of address of store
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Phone number of store
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Some description about store
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Store star
        /// </summary>
        public int Star { get; set; }

        /// <summary>
        /// Maximum customer of store can serve for a moment
        /// </summary>
        public int CustomerNumber { get; set; }
        
        /// <summary>
        /// List of menu item
        /// </summary>
        public ICollection<Item> Items { get; set; }
        
        /// <summary>
        /// List of image of store
        /// </summary>
        public ICollection<Image> Images { get; set; }
        
        /// <summary>
        /// Location of store
        /// </summary>
        public Location Location { get; set; }
        
        /// <summary>
        /// Location Id
        /// </summary>
        public Guid LocationId { get; set; }
        
        /// <summary>
        /// Food region of store
        /// </summary>
        public FoodRegion FoodRegion { get; set; }
        
        /// <summary>
        /// Food region id
        /// </summary>
        public Guid FoodRegionId { get; set; }
        
        /// <summary>
        /// Store type of store
        /// </summary>
        public StoreType StoreType { get; set; }
        
        /// <summary>
        /// Store type id
        /// </summary>
        public Guid StoreTypeId { get; set; }
        
        /// <summary>
        /// Status of store
        /// </summary>
        public Status Status { get; set; }
        
        /// <summary>
        /// Status id
        /// </summary>
        public  Guid StatusId { get; set; }

    }
}