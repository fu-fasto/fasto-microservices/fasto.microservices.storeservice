﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class DeleteOneRegion : CommandBase
    {
        public Guid Id { get; set; }
    }
}