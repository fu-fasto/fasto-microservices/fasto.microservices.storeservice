﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class DeleteOneStoreType: CommandBase
    {
        public Guid Id { get; set; }
    }
}