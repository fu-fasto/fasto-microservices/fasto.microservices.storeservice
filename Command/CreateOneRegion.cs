﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class CreateOneRegion : CommandBase
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public String RegionName { get; set; }
    }
}