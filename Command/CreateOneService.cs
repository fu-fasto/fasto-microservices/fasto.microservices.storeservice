﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class CreateOneService : CommandBase 
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public String ServiceName { get; set; }
    }
}