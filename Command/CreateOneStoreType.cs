﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class CreateOneStoreType: CommandBase
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public String Type { get; set; }
    }
}