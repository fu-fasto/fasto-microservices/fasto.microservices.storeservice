﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class DeleteOneStatusHandler: ICommandHandler<DeleteOneStatus>
    {
        private IRepository<Status, Guid> _statusRepository;

        public DeleteOneStatusHandler(IRepository<Status, Guid> statusRepository)
        {
            _statusRepository = statusRepository;
        }


        public async Task Handle(DeleteOneStatus command, CancellationToken cancellationToken)
        {
            await _statusRepository.DeleteOneAsync(command.Id, cancellationToken);
        }
    }
}