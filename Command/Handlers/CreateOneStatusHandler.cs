﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class CreateOneStatusHandler : ICommandHandler<CreateOneStatus>
    {
        private IRepository<Status, Guid> _statusRepository;

        public CreateOneStatusHandler(IRepository<Status, Guid> statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public async Task Handle(CreateOneStatus command, CancellationToken cancellationToken)
        {
            var status = command.MapTo<Status>();
            await _statusRepository.AddAsync(status, cancellationToken);
        }
    }
}