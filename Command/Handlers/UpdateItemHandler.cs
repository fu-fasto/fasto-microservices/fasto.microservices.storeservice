﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class UpdateItemHandler : ICommandHandler<UpdateItem>
    {
        private IRepository<Item, Guid> _itemRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateItemHandler(IRepository<Item, Guid> itemRepository, IUnitOfWork unitOfWork)
        {
            _itemRepository = itemRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(UpdateItem command, CancellationToken cancellationToken)
        {
            var item = await _itemRepository.FindOneAsync(command.Id, cancellationToken);
            item.Name = command.Name;
            item.Description = command.Description;
            item.Price = command.Price;
            item.ImagesUrl = command.ImagesUrl;
            item.StoreId = command.StoreId;
            
            using var trans = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _itemRepository.UpdateAsync(item, cancellationToken);
                await trans.CommitAsync(cancellationToken); }
            catch (Exception)
            {
                await trans.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}