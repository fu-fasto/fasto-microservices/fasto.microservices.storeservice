﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class CreateOneRegionHandler : ICommandHandler<CreateOneRegion>
    {
        private IRepository<FoodRegion, Guid> _regionRepository;

        public CreateOneRegionHandler(IRepository<FoodRegion, Guid> regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public async Task Handle(CreateOneRegion command, CancellationToken cancellationToken)
        {
            var region = command.MapTo<FoodRegion>();
            await _regionRepository.AddAsync(region, cancellationToken);
        }
    }
}