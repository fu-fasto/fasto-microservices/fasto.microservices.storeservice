﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class AddStoreInfoHandler : ICommandHandler<AddStoreInfo>
    {
        private IRepository<Item, Guid> _itemRepository;
        private IRepository<Image, Guid> _imageRepository;

        public AddStoreInfoHandler(IRepository<Item, Guid> itemRepository, IRepository<Image, Guid> imageRepository)
        {
            _itemRepository = itemRepository;
            _imageRepository = imageRepository;
        }

        public async Task Handle(AddStoreInfo command, CancellationToken cancellationToken)
        {
            var storeId = command.StoreId;
            foreach (var image in command.Images)
            {
                image.StoreId = storeId;
                await _imageRepository.AddAsync(image, cancellationToken);
            }
            
            foreach (var item in command.Items)
            {
                item.StoreId = storeId;
                await _itemRepository.AddAsync(item, cancellationToken);
            }
        }
    }
}