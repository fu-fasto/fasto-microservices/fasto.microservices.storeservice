﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class CreateOneStoreTypeHandler : ICommandHandler<CreateOneStoreType>
    {
        private IRepository<StoreType, Guid> _typeRepository;

        public CreateOneStoreTypeHandler(IRepository<StoreType, Guid> typeRepository)
        {
            _typeRepository = typeRepository;
        }

        public async Task Handle(CreateOneStoreType command, CancellationToken cancellationToken)
        {
            var type = command.MapTo<StoreType>();
            await _typeRepository.AddAsync(type, cancellationToken);
        }
    }
}