﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class UpdateStoreHandler : ICommandHandler<UpdateStore>
    {
        private IRepository<Store, Guid> _storeRepository;
        private IRepository<Location, Guid> _locationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateStoreHandler(IRepository<Store, Guid> storeRepository, 
            IRepository<Location, Guid> locationRepository, IUnitOfWork unitOfWork)
        {
            _storeRepository = storeRepository;
            _locationRepository = locationRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(UpdateStore command, CancellationToken cancellationToken)
        {
            var store = await _storeRepository.FindOneAsync(command.Id, cancellationToken);
            store.Name = command.Name;
            store.OpenTime = command.OpenTime;
            store.CloseTime = command.CloseTime;
            store.Address = command.Address;
            store.PhoneNumber = command.PhoneNumber;
            store.Description = command.Description;
            store.Star = command.Star;
            store.CustomerNumber = command.CustomerNumber;
            store.FoodRegionId = command.FoodRegionId;
            store.StoreTypeId = command.StoreTypeId;
            store.StatusId = command.StatusId;
            store.LocationId = command.LocationId;

            var location = await _locationRepository.FindOneAsync(command.LocationId, cancellationToken);
            location.City = command.City;
            location.District = command.District;
            location.Ward = command.Ward;
            
            using var trans = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _storeRepository.UpdateAsync(store, cancellationToken);
                await _locationRepository.UpdateAsync(location, cancellationToken);
                await trans.CommitAsync(cancellationToken); }
            catch (Exception)
            {
                await trans.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}