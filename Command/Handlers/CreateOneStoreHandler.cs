﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class CreateOneStoreHandler : ICommandHandler<CreateOneStore>
    {
        private IRepository<Store, Guid> _storeRepository;
        private IRepository<Location, Guid> _locationRepository;

        public CreateOneStoreHandler(IRepository<Store, Guid> storeRepository, 
            IRepository<Location, Guid> locationRepository)
        {
            _storeRepository = storeRepository;
            _locationRepository = locationRepository;
        }

        public async Task Handle(CreateOneStore command, CancellationToken cancellationToken)
        {
            //Todo: check object exist in dtb
            var location = new Location();
            location.City = command.City;
            location.District = command.District;
            location.Ward = command.Ward;
            await _locationRepository.AddAsync(location, cancellationToken);

            var store = command.MapTo<Store>();
            store.Location = location;
            store.LocationId = location.Id;
            await _storeRepository.AddAsync(store, cancellationToken);
            
            // foreach (var image in command.Images)
            // {
            //     image.StoreId = store.Id;
            //     await _imageRepository.AddAsync(image, cancellationToken);
            // }
            //
            // foreach (var item in command.Items)
            // {
            //     item.StoreId = store.Id;
            //     await _itemRepository.AddAsync(item, cancellationToken);
            // }
            //
            // foreach (var id in command.ServiceIds)
            // {
            //     StoreService storeService = new StoreService();
            //     storeService.StoreId = store.Id;
            //     storeService.ServiceId = id;
            //     await _storeServiceRepository.AddAsync(storeService, cancellationToken);
            // }
            
        }
    }
}