﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class DeleteOneRegionHandler : ICommandHandler<DeleteOneRegion>
    {
        private IRepository<FoodRegion, Guid> _regionRepository;

        public DeleteOneRegionHandler(IRepository<FoodRegion, Guid> regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public async Task Handle(DeleteOneRegion command, CancellationToken cancellationToken)
        {
            await _regionRepository.DeleteOneAsync(command.Id, cancellationToken);
        }
    }
}