﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Command.Handlers
{
    public class DeleteOneStoreTypeHandler: ICommandHandler<DeleteOneStoreType>
    {
        private IRepository<StoreType, Guid> _storeTypeRepository;

        public DeleteOneStoreTypeHandler(IRepository<StoreType, Guid> storeTypeRepository)
        {
            _storeTypeRepository = storeTypeRepository;
        }
        
        public async Task Handle(DeleteOneStoreType command, CancellationToken cancellationToken)
        {
            await _storeTypeRepository.DeleteOneAsync(command.Id);
        }
    }
}