﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class UpdateItem : CommandBase
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public double Price { get; set; }
        
        public string Description { get; set; }

        public string ImagesUrl { get; set; }
        
        public Guid StoreId { get; set; }
    }
}