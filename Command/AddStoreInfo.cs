﻿using System;
using System.Collections.Generic;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class AddStoreInfo : CommandBase
    {
        public Guid StoreId { get; set; }

        public ICollection<Item> Items { get; set; }
        
        public ICollection<Image> Images { get; set; }
        
        public ICollection<Guid> ServiceIds { get; set; }
    }
}