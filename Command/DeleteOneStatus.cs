﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class DeleteOneStatus : CommandBase
    {
        public Guid Id { get; set; }
    }
}