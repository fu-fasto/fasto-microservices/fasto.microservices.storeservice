﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class CreateOneStatus : CommandBase
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public string StatusName { get; set; }
    }
}