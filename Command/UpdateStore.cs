﻿using System;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.Storeservice.Command
{
    public class UpdateStore : CommandBase
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public DateTimeOffset OpenTime { get; set; }

        public DateTimeOffset CloseTime { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Description { get; set; }

        public int Star { get; set; }

        public int CustomerNumber { get; set; }

        public string City { get; set; }
        
        public string District { get; set; }
        
        public string Ward { get; set; }
        
        public Guid LocationId { get; set; }
        
        public Guid FoodRegionId { get; set; }
        
        public Guid StoreTypeId { get; set; }
        
        public  Guid StatusId { get; set; }
    }
}