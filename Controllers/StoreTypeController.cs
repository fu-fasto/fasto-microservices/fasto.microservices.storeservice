﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.Command;
using FastO.Microservices.Storeservice.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Storeservice.Controllers
{
    [ApiController]
    [Route("storeType")]
    public class StoreTypeController : ControllerBase
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;

        public StoreTypeController(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStoreType(CancellationToken cancellationToken)
        {
            var storeTypes = await _queryBus.SendAsync(new GetAllStoreType(), cancellationToken);
            return Ok(storeTypes);
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateOneStoreType([FromBody] CreateOneStoreType command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteOneStoreType([FromQuery] DeleteOneStoreType command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
    }
}