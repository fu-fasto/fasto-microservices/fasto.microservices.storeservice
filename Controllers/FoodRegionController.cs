﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.Command;
using FastO.Microservices.Storeservice.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Storeservice.Controllers
{
    [ApiController]
    [Route("foodRegion")]
    public class FoodRegionController : ControllerBase
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;
        
        public FoodRegionController(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllFoodRegion(CancellationToken cancellationToken)
        {
            var services = await _queryBus.SendAsync(new GetAllFoodRegion(), cancellationToken);
            return Ok(services);
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateOneRegion([FromBody] CreateOneRegion command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteOneRegion([FromQuery] DeleteOneRegion command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
    }
}