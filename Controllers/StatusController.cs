﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.Command;
using FastO.Microservices.Storeservice.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Storeservice.Controllers
{
    [ApiController]
    [Route("status")]
    public class StatusController: ControllerBase
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;

        public StatusController(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStatus(CancellationToken cancellationToken)
        {
            var status = await _queryBus.SendAsync(new GetAllStatus(), cancellationToken);
            return Ok(status);
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateOneStatus([FromBody] CreateOneStatus command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteOneService([FromQuery] DeleteOneStatus command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
    }
}