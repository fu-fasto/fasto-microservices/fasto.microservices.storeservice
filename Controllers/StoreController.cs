﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.Command;
using FastO.Microservices.Storeservice.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Storeservice.Controllers
{
    [ApiController]
    [Route("store")]
    public class StoreController : ControllerBase
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;

        public StoreController(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStores(CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetAllStore(), cancellationToken);
            return Ok(result);
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneStore([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneStore {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpPost("createStore")]
        public async Task<IActionResult> CreateStore([FromBody] CreateOneStore command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPost("addInfo")]
        public async Task<IActionResult> AddInfo([FromBody] AddStoreInfo command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.StoreId);
        }

        [HttpPost("search")]
        public async Task<IActionResult> SearchStore([FromQuery] SearchStore query, CancellationToken cancellationToken)
        {
            var stores = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(stores);
        } 
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStore([FromRoute] Guid id,
            [FromBody] UpdateStore command, 
            CancellationToken cancellationToken)
        {
            command.Id = id;
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
        
    }
}