﻿using System;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Storeservice.Queries
{
    public class GetAllItemOfStore : AllQueryBase<Item>
    {
        public Guid StoreId { get; set; }
    }
}