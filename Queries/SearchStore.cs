﻿using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Storeservice.Queries
{
    public class SearchStore : AllQueryBase<Store>
    {
        public string StoreName { get; set; }
    }
}