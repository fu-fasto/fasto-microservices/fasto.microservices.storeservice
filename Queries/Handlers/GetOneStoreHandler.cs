﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetOneStoreHandler : IQueryHandler<GetOneStore, Store>
    {
        private readonly IRepository<Store, Guid> _storeRepository;
        private readonly IRepository<Location, Guid> _locationRepository;
        private readonly IRepository<Image, Guid> _imageRepository;
        private readonly IRepository<StoreType, Guid> _typeRepository;
        private readonly IRepository<FoodRegion, Guid> _regionRepository;
        private readonly IRepository<Status, Guid> _statusRepository;
        private readonly IRepository<Item, Guid> _itemRepository;
        
        
        public GetOneStoreHandler(IRepository<Store, Guid> storeRepository, 
            IRepository<Location, Guid> locationRepository, 
            IRepository<Image, Guid> imageRepository, 
            IRepository<StoreType, Guid> typeRepository, 
            IRepository<FoodRegion, Guid> regionRepository, 
            IRepository<Status, Guid> statusRepository, 
            IRepository<Item, Guid> itemRepository
        )
        {
            _storeRepository = storeRepository;
            _locationRepository = locationRepository;
            _imageRepository = imageRepository;
            _typeRepository = typeRepository;
            _regionRepository = regionRepository;
            _statusRepository = statusRepository;
            _itemRepository = itemRepository;
        }

        public async Task<Store> Handle(GetOneStore query, CancellationToken cancellationToken)
        {
            var store = await _storeRepository.FindOneAsync(query.Id, cancellationToken)
                        ?? throw new NullReferenceException($"Store with Id: {query.Id} does not exist!");
            store.Location = await _locationRepository.FindOneAsync(store.LocationId, cancellationToken);
            store.StoreType = await _typeRepository.FindOneAsync(store.StoreTypeId, cancellationToken);
            store.FoodRegion = await _regionRepository.FindOneAsync(store.FoodRegionId, cancellationToken);
            store.Status = await _statusRepository.FindOneAsync(store.StatusId, cancellationToken);
            store.Images = (ICollection<Image>) await _imageRepository.FindAsync(null, cancellationToken);
            
            //find all images
            var findImages = new FindQuery<Image>
            {
                Size = 50
            };
            findImages.Filters.Add(x => x.StoreId.Equals(store.Id));
            store.Images = (ICollection<Image>) await _imageRepository.FindAsync(findImages, cancellationToken);

            //find all items
            var findItems = new FindQuery<Item>
            {
                Size = 200
            };
            findItems.Filters.Add(x => x.StoreId.Equals(store.Id));
            store.Items = (ICollection<Item>) await _itemRepository.FindAsync(findItems, cancellationToken);
            
            //find all service of store
            // var findStoreServices = new FindQuery<StoreService>
            // {
            //     Filter = x => x.StoreId.Equals(store.Id),
            //     Size = 20
            // };
            // store.StoreServices = (ICollection<StoreService>) await _storeServiceRepository.FindAsync(findStoreServices, cancellationToken);
            
            // foreach (var ss in store.StoreServices)
            // {
            //     ss.Service = await _serviceRepository.FindOneAsync(ss.ServiceId, cancellationToken);
            // }
            
            return store;
        }
    }
}