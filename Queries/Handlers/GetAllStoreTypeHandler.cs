﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetAllStoreTypeHandler : IQueryHandler<GetAllStoreType, IEnumerable<StoreType>>
    {
        private readonly IRepository<StoreType, Guid> _storeTypeRepository;

        public GetAllStoreTypeHandler(IRepository<StoreType, Guid> storeTypeRepository)
        {
            _storeTypeRepository = storeTypeRepository;
        }

        public async Task<IEnumerable<StoreType>> Handle(GetAllStoreType query, CancellationToken cancellationToken)
        {
            var storeTypes = await _storeTypeRepository.FindAsync(null, cancellationToken);
            return storeTypes;
        }
    }
}