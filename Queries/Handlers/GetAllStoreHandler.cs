﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.EntityFrameworkCore;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetAllStoreHandler : IQueryHandler<GetAllStore, IEnumerable<Store>>
    {
        private readonly IRepository<Store, Guid> _storeRepository;
        private readonly IRepository<Location, Guid> _locationRepository;
        private readonly IRepository<StoreType, Guid> _typeRepository;
        private readonly IRepository<Status, Guid> _statusRepository;
        public GetAllStoreHandler(IRepository<Store, Guid> storeRepository, IRepository<Location, Guid> locationRepository, IRepository<Image, Guid> imageRepository, IRepository<StoreType, Guid> typeRepository, IRepository<FoodRegion, Guid> regionRepository, IRepository<Status, Guid> statusRepository, IRepository<Item, Guid> itemRepository)
        {
            _storeRepository = storeRepository;
            _locationRepository = locationRepository;
            _typeRepository = typeRepository;
            _statusRepository = statusRepository;
        }

        public async Task<IEnumerable<Store>> Handle(GetAllStore query, CancellationToken cancellationToken)
        {
            var stores = await _storeRepository.FindAsync(null, cancellationToken);
            foreach (var store in stores)
            {
                store.Location = await _locationRepository.FindOneAsync(store.LocationId, cancellationToken);
                store.StoreType = await _typeRepository.FindOneAsync(store.StoreTypeId, cancellationToken);
                // store.FoodRegion = await _regionRepository.FindOneAsync(store.FoodRegionId, cancellationToken);
                store.Status = await _statusRepository.FindOneAsync(store.StatusId, cancellationToken);
            }
            return stores;
        }
    }
}