﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class SearchStoreHandler : IQueryHandler<SearchStore, IEnumerable<Store>>
    {
        private readonly IRepository<Store, Guid> _storeRepository;
        private readonly IRepository<Location, Guid> _locationRepository;
        private readonly IRepository<StoreType, Guid> _typeRepository;
        private readonly IRepository<Status, Guid> _statusRepository;
        private readonly IRepository<FoodRegion, Guid> _foodRegionRepository;

        public SearchStoreHandler(IRepository<Store, Guid> storeRepository, IRepository<Location, Guid> locationRepository, IRepository<StoreType, Guid> typeRepository, IRepository<Status, Guid> statusRepository, IRepository<FoodRegion, Guid> foodRegionRepository)
        {
            _storeRepository = storeRepository;
            _locationRepository = locationRepository;
            _typeRepository = typeRepository;
            _statusRepository = statusRepository;
            _foodRegionRepository = foodRegionRepository;
        }

        public async Task<IEnumerable<Store>> Handle(SearchStore query, CancellationToken cancellationToken)
        {
            var findItems = new FindQuery<Store>
            {
                Size = 200
            };
            findItems.Filters.Add(x => x.Name.Contains(query.StoreName));

            var stores = await _storeRepository.FindAsync(findItems, cancellationToken);
            foreach (var store in stores)
            {
                store.Location = await _locationRepository.FindOneAsync(store.LocationId, cancellationToken);
                store.StoreType = await _typeRepository.FindOneAsync(store.StoreTypeId, cancellationToken);
                store.Status = await _statusRepository.FindOneAsync(store.StatusId, cancellationToken);
                store.FoodRegion = await _foodRegionRepository.FindOneAsync(store.FoodRegionId, cancellationToken);
            }

            return stores;
        }
    }
}