﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetOneItemHandler : IQueryHandler<GetOneItem, Item>
    {
        private readonly IRepository<Item, Guid> _itemRepository;

        public GetOneItemHandler(IRepository<Item, Guid> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<Item> Handle(GetOneItem query, CancellationToken cancellationToken)
        {
            var item = await _itemRepository.FindOneAsync(query.Id, cancellationToken);
            return item;
        }
    }
}