﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetAllStatusHandler : IQueryHandler<GetAllStatus, IEnumerable<Status>>
    {
        private readonly IRepository<Status, Guid> _statusRepository;

        public GetAllStatusHandler(IRepository<Status, Guid> statusRepository)
        {
            _statusRepository = statusRepository;
        }
        
        public async Task<IEnumerable<Status>> Handle(GetAllStatus command, CancellationToken cancellationToken)
        {
            var status =  await _statusRepository.FindAsync(null, cancellationToken);
            return status;
        }
    }
}