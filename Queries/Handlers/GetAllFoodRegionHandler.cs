﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Storeservice.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Storeservice.Queries.Handlers
{
    public class GetAllFoodRegionHandler : IQueryHandler<GetAllFoodRegion, IEnumerable<FoodRegion>>
    {
        private readonly IRepository<FoodRegion, Guid> _regionRepository;

        public GetAllFoodRegionHandler(IRepository<FoodRegion, Guid> regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public async Task<IEnumerable<FoodRegion>> Handle(GetAllFoodRegion query, CancellationToken cancellationToken)
        {
            var foodRegions = await _regionRepository.FindAsync(null, cancellationToken);
            return foodRegions;
        }
    }
}